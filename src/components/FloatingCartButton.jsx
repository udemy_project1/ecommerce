import React from "react";
import { useState } from "react";
import shoppingCart from "../assets/shopping-cart.svg";
import { useSelector, useDispatch } from "react-redux";
import { createPortal } from "react-dom";
import Cart from "./Cart";

export default function FloatingCartButton() {
	const [showModal, setShowModal] = useState(false);

	const cartValues = useSelector((state) => state.cart);
	return (
		<>
			<button
				onClick={() => setShowModal(!showModal)}
				className="fixed py-2 px-4 top-5 right-5 rounded bg-slate-100 flex justify-center items-center">
				<img
					src={shoppingCart}
					alt="Shopping cart icon"
					className="w-6 h-6 mr-4"
				/>
				<span className="mr-2">View your cart</span>
				<span className="text-lg font-semibold"> {cartValues.cartItems.length}</span>
			</button>
			{showModal && createPortal(<Cart onClose={() => setShowModal(false)} />, document.body)}
		</>
	);
}
