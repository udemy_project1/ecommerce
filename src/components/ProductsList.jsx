import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { getProductsList } from "../features/products";
import { addOneToCart } from "../features/cart";

export default function ProductsList() {
	const dispatch = useDispatch();
	const productsData = useSelector((state) => state.products);

	if (!productsData.items) {
		dispatch(getProductsList());
	}
	console.log(productsData);
	return (
		<div className="px-6">
			<h1 className="text-slate-100 text-2xl mb-6">Here are our products</h1>
			<div className="grid min-[500px]:grid-cols-2 md:grid-cols-3 gap-4">
				{productsData.items &&
					productsData.items.map((product) => (
						<li
							key={product.id}
							className="p-4 bg-slate-200 rounded list-none">
							<img
								src={`/images/${product.img}.png`}
								alt={product.title}
								className="mb-4"
							/>
							<div className="flex justify-between items-center mb-6">
								<p className="text-slate-700 text-lg">{product.title}</p>
								<p className="text-slate-900 font-bold">{product.price}</p>
							</div>
							<button
								onClick={() => dispatch(addOneToCart(product.id))}
								className={`${product.picked ? "bg-green-700" : "bg-slate-600"} w-full text-slate-100 p-2 mr-2 inline-flex items-center justify-center rounded`}>
								{product.picked ? "Item picked" : "Add to cart"}
							</button>
						</li>
					))}
			</div>
		</div>
	);
}
