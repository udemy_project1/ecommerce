import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	items: undefined,
};

const productsSlice = createSlice({
	name: "products",
	initialState,
	reducers: {
		addProducts: (state, actions) => {
			state.items = actions.payload;
		},
	},
	extraReducers: {
		["cart/createCartItem"]: (state, action) => {
			state.items.find((el) => el.id === action.payload.id).picked = true;
		},
		["cart/deleteFromCart"]: (state, action) => {
			state.items.find((el) => el.id === action.payload).picked = false;
		},
	},
});

export function getProductsList(action) {
	return function (dispatch, getState) {
		fetch(`/data/inventory.json`)
			.then((response) => response.json())
			.then((data) => dispatch(addProducts(data.products)));
	};
}

export const { addProducts } = productsSlice.actions;

export default productsSlice.reducer;
